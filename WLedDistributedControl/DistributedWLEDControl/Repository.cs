﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts;

namespace DistributedWLEDControl
{
    public class Repository
    {
        public static Repository Access = new Repository();
        public List<Participant> Participants;
        public Participant Myself;

        public Repository()
        {
            Myself= new Participant
            {
                Coordinator = true,
                ID = Guid.NewGuid(),
                Address = $"soap.udp://{System.Net.Dns.GetHostName()}:9999/NegotiationService"
            };
        }
    }
}
