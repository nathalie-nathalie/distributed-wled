﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace DistributedWLEDControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string ipAddress;
        private WledManager wledManager;
        public MainWindow()
        {
            InitializeComponent();
            wledManager = new WledManager(this);
            ConnectionManager.Access.Instantiate();
        }
        

        private void _ipAddressChanged(object sender, TextChangedEventArgs e)
        {
            ipAddress = _ipAddressTextBox.Text;
            _ipAddressTextBox.Foreground = Brushes.Red;
            Match match = Regex.Match(_ipAddressTextBox.Text, @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
            if (match.Success)
            {
                _ipAddressTextBox.Foreground = Brushes.Black;
                _confirmAddress.IsEnabled = true;
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            wledManager.SetAddress(ipAddress);
        }

        private void _colorPicker_SelectedColorChanged(object sender, EventArgs e)
        {
            string color = _colorPicker.SelectedColor.ToString().Replace("#", "h");
            System.Diagnostics.Debug.WriteLine("picked color: "+color);
            ConnectionManager.Access.SetColor(color);
        }

        public void SetLedConnectionStatus(string status, SolidColorBrush color)
        {
            tb_LEDConnection.Foreground = color;
            tb_LEDConnection.Text=status;
        }
    }
}
