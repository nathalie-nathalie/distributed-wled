﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts;

namespace DistributedWLEDControl
{
    class Bully
    {
        public static Bully Access = new Bully();

        public void StartVote(List<Participant> participants)
        {
            System.Diagnostics.Debug.WriteLine("Coordinator-election started");

            var myselfParticipant = Repository.Access.Myself;
            var higherParticipant = false;
            var higherParticipants= participants.FindAll(participant => participant.Priority > myselfParticipant.Priority);

            var respondingParticipants = new List<Participant>();
            higherParticipants.ForEach(participant =>
            {
                var respondingParticipant = ConnectionManager.Access.SendElectionToParticipant(participant.Address);
                if (respondingParticipant != null)
                {
                    higherParticipant = true;
                    respondingParticipants.Add(respondingParticipant);
                }
            });
            RemoveSilentParticipants(higherParticipants, respondingParticipants);
            if (higherParticipant)
            {
                var coordinator = ChooseCoordinator(respondingParticipants);
                AnnounceCoordinator(coordinator);
                return;
            }
            AnnounceYourselfAsCoordinator();
        }

        private void RemoveSilentParticipants(List<Participant> higherParticipants, List<Participant> respondingParticipants)
        {
            higherParticipants.ForEach(higherParticipant =>
            {
                if (!respondingParticipants.Exists(respondingParticipant => respondingParticipant.Equals(higherParticipant)))
                {
                    var index = Repository.Access.Participants.FindIndex(p => p.Equals(higherParticipant));
                    Repository.Access.Participants.RemoveAt(index);
                }
            });
        }

        private Participant ChooseCoordinator(List<Participant> respondingParticipants)
        {
            var maxPriority= respondingParticipants.Max(participant => participant.Priority);
            var coordinator = respondingParticipants.Find(participant => participant.Priority == maxPriority);
            return coordinator;
        }

        private void AnnounceYourselfAsCoordinator()
        {
            System.Diagnostics.Debug.WriteLine("I am coordinator");

            Repository.Access.Myself.Coordinator = true;
            AnnounceCoordinator(Repository.Access.Myself);
        }

        private void AnnounceCoordinator(Participant coordinator) => ConnectionManager.Access.AnnounceCoordinatorToEveryone(coordinator);

    }
}
