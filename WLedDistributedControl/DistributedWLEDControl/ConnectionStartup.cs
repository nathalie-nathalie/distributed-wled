﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Discovery;

namespace DistributedWLEDControl
{
    class ConnectionStartup
    {
        private ServiceHost server;
        private string GetMyIpAdress() => System.Net.Dns.GetHostName();

        public void StartService()
        {
            server = new ServiceHost(typeof(NegotiationService), new Uri($"soap.udp://{GetMyIpAdress()}:9999/NegotiationService"));

            ServiceDiscoveryBehavior discoveryBehavior = new ServiceDiscoveryBehavior();

            server.Description.Behaviors.Add(discoveryBehavior);

            discoveryBehavior.AnnouncementEndpoints.Add(new UdpAnnouncementEndpoint());

            server.Description.Endpoints.Add(new UdpDiscoveryEndpoint());

            server.Open();

            System.Diagnostics.Debug.WriteLine("Your service is started...");
        }


        public void CloseServer()
        {
            server.Close();
        }
           
    }
    
    
}
