﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Discovery;
using Contracts;

namespace DistributedWLEDControl
{
    class ConnectionDiscoverer
    {
        private DiscoveryClient _discoveryClient;


        internal IReadOnlyCollection<EndpointAddress> FindService()
        {
            _discoveryClient =
                new DiscoveryClient(new UdpDiscoveryEndpoint());

            var services = _discoveryClient.Find(new FindCriteria(typeof(INegotiationService))).Endpoints;

            var result = services.Select(s => s.Address).ToList();

            CloseConnection();

            return result;
        }


        public void CloseConnection()
        {
            _discoveryClient.Close();
        }

    }
}