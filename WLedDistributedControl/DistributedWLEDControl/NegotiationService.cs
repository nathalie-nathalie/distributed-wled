﻿using System;
using System.Collections.Generic;
using System.Net;
using Contracts;

namespace DistributedWLEDControl
{
    public class NegotiationService : INegotiationService
    {
        public List<Participant> ShowParticipants()=> Repository.Access.Participants;

        public Participant Election() => Repository.Access.Myself;

        private Clock _clientClock = new Clock();

        public void UpdateParticipants(List<Participant> participants)
        {
            Repository.Access.Participants = participants;
            Repository.Access.Myself = participants.Find(p => p.Address.ToLower().Contains(Dns.GetHostName().ToLower()));
        }

        public void AnnounceCoordinator(Participant coordinator)
        {
            Repository.Access.Participants.ForEach(p =>p.Coordinator=false);
            if (!Repository.Access.Participants.Exists(p => p.ID.Equals(coordinator.ID)))
            {
                Repository.Access.Participants.Add(coordinator);
                return;
            };
            Repository.Access.Participants.ForEach(p =>
            {
                if (p.ID.Equals(coordinator.ID))
                {
                    p.Coordinator = true;
                }
            });
        }

        public TimeSpan CalculateClientOffset(DateTime coordinatorTime)
        {
            System.Diagnostics.Debug.WriteLine("My offset from coordinator: " + _clientClock.GetStartTime().Subtract(coordinatorTime));
            return _clientClock.GetStartTime().Subtract(coordinatorTime);
        }

        public void SetAverageTime(DateTime time)
        {
            _clientClock.ClientOffsetFromAvg(time);
            System.Diagnostics.Debug.WriteLine("My offset from average: " + time);
        }

        public bool AreYouUp() => true;

        public void SetLed(string color, DateTime timeStamp)
        {
            WledManager.Access.SetColor(color, timeStamp);
        }
    }
}