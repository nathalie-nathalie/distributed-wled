﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DistributedWLEDControl
{
    public class WledManager
    {
        public WledManager(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
            Access = this;
        }

        public static WledManager Access;
        private readonly MainWindow _mainWindow;
        private string _address;
        private bool _isAddressValid;
        private string _lastColor;
        private DateTime _lastTime;

        public string GetLastColor()
        {
            return _lastColor;
        }

        public DateTime GetLastTime()
        {
            return _lastTime;
        }

        public void SetAddress(string ip)
        {
            _address = $"http://{ip}/";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"{_address}win&CL=h00ff00");
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                _isAddressValid = response.StatusCode == HttpStatusCode.OK;
                response.Close();
                _mainWindow.SetLedConnectionStatus("WLED is ready", Brushes.ForestGreen);

            }
            catch (Exception e)
            {
                _mainWindow.SetLedConnectionStatus("no WLED connected", Brushes.RoyalBlue);
                System.Diagnostics.Debug.WriteLine("Wled could not be found");

            }

        }

        public void SetColor(string color, DateTime timeStamp)
        {
            if (_isAddressValid && timeStamp >= _lastTime)
            {
                try
                {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"{_address}win&CL={color}");
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                response.Close();
                System.Diagnostics.Debug.WriteLine("Set LED Color: " + color);
                _lastTime = timeStamp;
                return;
                }
                catch (Exception e)
                {
                    handleOffline(color, timeStamp);
                }
            }
        }

        private void handleOffline(string color, DateTime timeStamp)
        {
            _lastColor = color;
            _lastTime = timeStamp;
            ConnectionManager.Access.setOffline(true);
            System.Diagnostics.Debug.WriteLine("Color can not be set, we're offline");

        }
    }
}
