﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Contracts;

namespace DistributedWLEDControl
{
    class ConnectionManager
    {
        private List<Participant> _mergedParticipants;
        private Clock _clock = new Clock();
        private bool _iWasOffline;

        private bool AmICoordinator() => Repository.Access.Myself.Coordinator;
        private bool AmITalkingToMyself(EndpointAddress serviceAddress) => serviceAddress.Uri.AbsoluteUri.ToLower().Contains(System.Net.Dns.GetHostName().ToLower());
        private bool AmITalkingToMyself(Participant target) => target.Address.ToLower().Contains(System.Net.Dns.GetHostName().ToLower());
        private bool AmIIncluded() => _mergedParticipants.Exists(p => p.Address.ToLower().Contains(System.Net.Dns.GetHostName().ToLower()));
        private string GetCoordinatorAddress() => Repository.Access.Participants.Find(p => p.Coordinator).Address;

        public static ConnectionManager Access = new ConnectionManager();

        public void setOffline(bool offline)
        {
            _iWasOffline = offline;
        }

        public bool WasIOffline()
        {
            return _iWasOffline;
        }

        public void Instantiate()
        {
            var startup = new ConnectionStartup();
            startup.StartService();

            _mergedParticipants = new List<Participant>();

            var discoverer = new ConnectionDiscoverer();
            foreach (var address in discoverer.FindService())
            {
                InvokeService(address);
            }

            AddYourselfToParticipants();
            Repository.Access.Participants = _mergedParticipants;
            SendUpdateToAllParticipants();
            StartSynchronization();
        }

        public void GoBackToOnline()
        {
            _mergedParticipants.Clear();
            System.Diagnostics.Debug.WriteLine("Trying to reconnect");
            var discoverer = new ConnectionDiscoverer();
            foreach (var address in discoverer.FindService())
            {
                InvokeService(address);
            }
            AddYourselfToParticipants();
            Repository.Access.Participants = _mergedParticipants;
            SendUpdateToAllParticipants();
            if (AmICoordinator())
            {
                StartSynchronization();
            }
            SetColor(WledManager.Access.GetLastColor(), WledManager.Access.GetLastTime().Add(_clock.GetOffset()));
        }

        internal void InvokeService(EndpointAddress serviceAddress)
        {
            if (AmITalkingToMyself(serviceAddress)) { return; }
            System.Diagnostics.Debug.WriteLine("\nInvoking Negotiation Service at {0}\n", serviceAddress);
            var binding = new UdpBinding();

            using (ChannelFactory<INegotiationService> factory = new ChannelFactory<INegotiationService>(binding, serviceAddress))
            {
                INegotiationService proxy = factory.CreateChannel();
                var currentParticipants = proxy.ShowParticipants();
                _mergedParticipants = _mergedParticipants.Union(currentParticipants).ToList();
            }
        }


        public void SetColor(string color)
        {
            SetColor(color, _clock.GetSendTimeWithOffset());
        }

        public void SetColor(string color, DateTime timeStamp)
        {
            if (AmICoordinator())
            {
                WledManager.Access.SetColor(color, timeStamp);
                return;
            }
            var binding = new UdpBinding();
            binding.SendTimeout = TimeSpan.FromSeconds(10);
            using (ChannelFactory<INegotiationService> factory =
                new ChannelFactory<INegotiationService>(binding, GetCoordinatorAddress()))
            {
                try
                {
                    INegotiationService proxy = factory.CreateChannel();
                    var up = proxy.AreYouUp();
                }
                catch (Exception e)
                {
                    Bully.Access.StartVote(Repository.Access.Participants);
                    SetColor(color, timeStamp);
                }

                try
                {
                    INegotiationService proxy = factory.CreateChannel();
                    proxy.SetLed(color, timeStamp);
                }
                catch (Exception e)
                {
                }
            }
        }

        public Participant SendElectionToParticipant(string address)
        {
            var binding = new UdpBinding();
            using (ChannelFactory<INegotiationService> factory = new ChannelFactory<INegotiationService>(binding, address))
            {
                try
                {
                    INegotiationService proxy = factory.CreateChannel();
                    var participant = proxy.Election();
                    return participant;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public void AnnounceCoordinatorToEveryone(Participant coordinator)
        {
            System.Diagnostics.Debug.WriteLine("New coordinator is announced");

            Repository.Access.Participants.ForEach(p => AnnounceCoordinatorToParticipant(coordinator, p));
            StartSynchronization();
        }

        private void AnnounceCoordinatorToParticipant(Participant coordinator, Participant target)
        {
            if (AmITalkingToMyself(target)) { return; }
            var binding = new UdpBinding();
            using (ChannelFactory<INegotiationService> factory = new ChannelFactory<INegotiationService>(binding, target.Address))
            {
                try
                {
                    INegotiationService proxy = factory.CreateChannel();
                    proxy.AnnounceCoordinator(coordinator);
                }
                catch (Exception e) { return; }
            }
        }

        public void StartSynchronization()
        {
            System.Diagnostics.Debug.WriteLine("Synchronization with Berkeley started");

            if (!AmICoordinator())
            {
                _clock.StopTimer();
                return;
            }
            _clock.SetStartTime();
            List<TimeSpan> times = new List<TimeSpan>();
            Repository.Access.Participants.ForEach(p => times.Add(SendTime(_clock.GetStartTime(), p)));

            DateTime averageTime = _clock.AverageTime(times);
            Repository.Access.Participants.ForEach(p => SendAverageTime(averageTime, p));
        }

        private void SendAverageTime(DateTime average, Participant target)
        {
            if (AmITalkingToMyself(target)) { return; }
            System.Diagnostics.Debug.WriteLine("Average time is: " + average);

            var binding = new UdpBinding();
            using (ChannelFactory<INegotiationService> ChannelFactory = new ChannelFactory<INegotiationService>(binding, target.Address))
            {
                try
                {
                    INegotiationService proxy = ChannelFactory.CreateChannel();
                    proxy.SetAverageTime(average);
                }
                catch (Exception e) { return; }
            }
        }

        private TimeSpan SendTime(DateTime time, Participant target)
        {
            if (AmITalkingToMyself(target)) { return TimeSpan.Zero; }
            var binding = new UdpBinding();
            using(ChannelFactory<INegotiationService> ChannelFactory = new ChannelFactory<INegotiationService>(binding, target.Address))
            {
                try
                {
                    INegotiationService proxy = ChannelFactory.CreateChannel();
                    return proxy.CalculateClientOffset(time);
                }
                catch(Exception e) { return TimeSpan.Zero; }
            }
        }

        private void AddYourselfToParticipants()
        {
            if (AmIIncluded()) return;
            var maxPriority = 0;
            if (_mergedParticipants.Count>0)
            {
                maxPriority = _mergedParticipants.Max(p => p.Priority);
                _mergedParticipants.ForEach(p => p.Coordinator = false);
            }

            Repository.Access.Myself.Priority = ++maxPriority;
            _mergedParticipants.Add(Repository.Access.Myself);
        }

        private void SendUpdateToAllParticipants() => 
            Repository.Access.Participants.ForEach(participant => SendUpdateToSpecificParticipant( participant));

        private void SendUpdateToSpecificParticipant(Participant participant)
        {
            if (AmITalkingToMyself(participant)) { return; }
            var binding = new UdpBinding();
            using (ChannelFactory<INegotiationService> factory = new ChannelFactory<INegotiationService>(binding, participant.Address))
            {
                INegotiationService proxy = factory.CreateChannel();
                proxy.UpdateParticipants(Repository.Access.Participants);
            }
        }
    }
}
