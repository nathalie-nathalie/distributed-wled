﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedWLEDControl
{
    class Clock
    {
        private DateTime startTime;
        private TimeSpan offset;
        private Timer aTimer = new Timer(30000);
        private bool TimerIsSetUp = false;

        public DateTime AverageTime(List<TimeSpan> times)
        {
            TimeSpan collectedOffset = TimeSpan.Zero;
            times.ForEach(t => collectedOffset += t);
            return startTime.Add(collectedOffset);
        }

        public void SynchronizeClocks()
        {
            if (!TimerIsSetUp)
            {
                aTimer.Elapsed += OnTimedEvent;
                aTimer.AutoReset = true;
                aTimer.Enabled = true;
                TimerIsSetUp = true;
            }
        }
        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            ConnectionManager.Access.StartSynchronization();
            if (ConnectionManager.Access.WasIOffline())
            {
                ConnectionManager.Access.GoBackToOnline();
            }
        }

        public Clock() => startTime = DateTime.Now;

        public void SetStartTime()
        {
            startTime = DateTime.Now;
            SynchronizeClocks();
        }

        public void StopTimer()
        {
            aTimer.Stop();
            aTimer.Dispose();
        }

        public DateTime GetSendTimeWithOffset()
        {
            return DateTime.Now.Add(offset);
        }

        public TimeSpan GetOffset() => offset;

        public void SetOffset(TimeSpan offset) => this.offset = offset;

        public DateTime GetStartTime() { return startTime; }

        public void ClientOffsetFromAvg(DateTime time) => offset = startTime.Subtract(time);
    }
}
