﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    [DataContract]
    public class Participant: IEquatable<Participant>
    {
        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public bool Coordinator { get; set; }
        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public DateTime Offset { get; set; }

        public bool Equals(Participant participant) => participant?.Address.Equals(Address) ?? false;
    }
}
