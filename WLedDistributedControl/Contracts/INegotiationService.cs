﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;

namespace Contracts
{
    [ServiceContract]
    public interface INegotiationService
    {
        [OperationContract]
        void UpdateParticipants(List<Participant> participants);

        [OperationContract]
        List<Participant> ShowParticipants();

        [OperationContract]
        Participant Election();

        [OperationContract]
        void AnnounceCoordinator(Participant coordinator);

        [OperationContract]
        TimeSpan CalculateClientOffset(DateTime time);

        [OperationContract]
        void SetAverageTime(DateTime time);

        [OperationContract]
        bool AreYouUp();

        [OperationContract]
        void SetLed(string color, DateTime timestamp);
    }
}